# Staging

A simple app to add some additional CSS in your staging instances.


## Install

```bash
# change to bench directory
cd ~/frappe-bench

# get this app
bench get-app https://git.fairkom.net/hosting/frappe/apps/staging.git

# install in your instance
bench --site <your site> install-app staging

# you will have to clear caches
bench --site <your site> clear-cache
bench --site <your site> clear-website-cache

# you might have to migrate
bench --site <your site> migrate
```


## License

MIT
