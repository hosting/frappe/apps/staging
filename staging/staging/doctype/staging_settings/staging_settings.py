# Copyright (c) 2023, Oliver Maklott and contributors
# For license information, please see license.txt

import os
import re

import frappe
from frappe.model.document import Document
from frappe.website.utils import (
	cleanup_page_name,
	clear_cache,
	clear_website_cache
)
from staging.functions import (
	str_empty,
	str_not_empty,
	get_app_dir,
	get_public_dir,
	calculate_element_colors
)

CSS_SELECTOR = re.compile('^(\s*)([\w \[\]=\.#"\'+*-]+{)', re.I)

class StagingSettings(Document):
	def validate(self):
		pass

	def on_update(self):

		my_host = None
		try:
			my_host = frappe.request.host
		except:
			return

		if my_host is None:
			return

		host_selector = '[data-host="{}"]'.format(my_host)
		
		desk_css_container = []
		web_css_container = []
		if str_not_empty(self.desk_background_color):
			variants = calculate_element_colors(self.desk_background_color)

			desk_css_container.append(
				host_selector + ':root { --bg-color: ' + self.desk_background_color + '; }'
			)
			desk_css_container.append(
				'body' + host_selector + ' { background-color: ' + self.desk_background_color + ' !important; }'
			)
			desk_css_container.append(
				host_selector + ' #body { background-color: ' + self.desk_background_color + ' !important; }'
			)
			desk_css_container.append(
				host_selector + ' #page-Workspaces { background-color: ' + self.desk_background_color + ' !important; }'
			)
			desk_css_container.append(
				host_selector + ' .page-head { background-color: ' + variants[0] + ' !important; }'
			)

		if str_not_empty(self.web_background_color):
			variants = calculate_element_colors(self.web_background_color)

			web_css_container.append(
				host_selector + ':root { --bg-color: ' + self.web_background_color + '; }'
			)
			web_css_container.append(
				'body' + host_selector + ' { background-color: ' + self.web_background_color + ' !important; }'
			)
			web_css_container.append(
				host_selector + ' #body { background-color: ' + self.web_background_color + ' !important; }'
			)
			web_css_container.append(
				host_selector + ' #page-Workspaces { background-color: ' + self.web_background_color + ' !important; }'
			)
			web_css_container.append(
				host_selector + ' .page-head { background-color: ' + variants[0] + ' !important; }'
			)

		if str_not_empty(self.desk_css):
			desk_css_container.append(self.desk_css)

		if str_not_empty(self.web_css):
			web_css_container.append(self.web_css)

		app_dir = get_app_dir()
		public_dir = get_public_dir()

		with open(os.path.join(public_dir, 'css/staging-desk-{}.css'.format(my_host)), 'w') as fh:
			fh.write("\n".join(desk_css_container))

		with open(os.path.join(public_dir, 'css/staging-web-{}.css'.format(my_host)), 'w') as fh:
			fh.write("\n".join(web_css_container))

		has_import = False
		with open(os.path.join(public_dir, 'css/staging-desk.css'), 'r') as fh:
			while True:
				line = fh.readline()
				if not line:
					break
				if line.find(my_host) > -1:
					has_import = True
					break
		if not has_import:
			with open(os.path.join(public_dir, 'css/staging-desk.css'), 'a') as fh:
				fh.write('@import "staging-desk-{}.css";'.format(my_host))
				fh.write("\n")

		has_import = False
		with open(os.path.join(public_dir, 'css/staging-web.css'), 'r') as fh:
			while True:
				line = fh.readline()
				if not line:
					break
				if line.find(my_host) > -1:
					has_import = True
					break
		if not has_import:
			with open(os.path.join(public_dir, 'css/staging-web.css'), 'a') as fh:
				fh.write('@import "staging-web-{}.css";'.format(my_host))
				fh.write("\n")

		self.clear_cache()


	def clear_cache(self):
		frappe.clear_cache()
		super().clear_cache()
		clear_cache()
		clear_website_cache()
