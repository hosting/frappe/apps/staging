# Copyright (c) 2022 Oliver Maklott
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import os
import re
import json
import datetime
import math
import colorsys

import frappe
from frappe.utils import (
	cint,
	get_assets_json,
	get_time_zone,
	md_to_html,
	get_site_name,
	strip_html
)


SRCSET_TTL = 60 * 60 * 24 # one day
EPOCH_DT = datetime.datetime(1970, 1, 1)


####### Jinja hooks setup
custom_jinja = {
	'methods': [

	],
	'filters': [
		
	]
}


@frappe.whitelist(allow_guest=True)
def str_empty(value):
	if value is None:
		return True
	
	if not isinstance(value, str):
		return True
	
	if len(value) > 0:
		return False
	return True


@frappe.whitelist(allow_guest=True)
def str_not_empty(value):
	if value is None:
		return False
	
	if not isinstance(value, str):
		return False
	
	if len(value) > 0:
		return True
	return False


@frappe.whitelist()
def get_app_dir():
	return os.path.dirname(os.path.dirname(__file__))

@frappe.whitelist()
def get_public_dir():
	return os.path.join(os.path.dirname(os.path.dirname(__file__)), 'staging/public')




#
# Calculate colors
#

@frappe.whitelist(allow_guest=True)
def to_hex(r, g, b):
	return (
		'#' + 
		('00' + hex(int(r * 255))[2:])[-2:] + 
		('00' + hex(int(g * 255))[2:])[-2:] + 
		('00' + hex(int(b * 255))[2:])[-2:]
	)


@frappe.whitelist(allow_guest=True)
def to_rgb(val):
	if not val:
		return None
	if val.startswith('#'):
		val = val[1:]
	if val.startswith('0x'):
		val = val[2:]
	r_val = int('0x' + val[0:2], base=16) / 255
	g_val = int('0x' + val[2:4], base=16) / 255
	b_val = int('0x' + val[4:6], base=16) / 255
	return [r_val, g_val, b_val]


@frappe.whitelist(allow_guest=True)
def calc_luminance(r, g, b):
	return math.sqrt((0.299*r*r) + (0.587*g*g) + (0.114*b*b))


@frappe.whitelist(allow_guest=True)
def calculate_element_colors(val):
	if not val:
		return

	container = []
	rgb_val = to_rgb(val)
	rgb_val_lum = calc_luminance(rgb_val[0], rgb_val[1], rgb_val[2])
	hls_val = colorsys.rgb_to_hls(rgb_val[0], rgb_val[1], rgb_val[2])

	hue = hls_val[0]
	lum = hls_val[1]
	sat = hls_val[2]
	sat_05 = sat * 0.25
	lum_lim = max(min(lum - 0.025, 1), 0)

	if rgb_val_lum < 0.5:
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum * 1.2, 1), 0), sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum + 1.4, 1), 0), sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum, sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum_lim, sat_05)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum + 0.25 - 0.025, 1), 0), sat_05)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum_lim, sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
	else:
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum / 1.2, 1), 0), sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum / 1.4, 1), 0), sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum, sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum_lim, sat_05)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, max(min(lum - 0.25 - 0.025, 1), 0), sat_05)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))
		rgb_tuple = colorsys.hls_to_rgb(hue, lum_lim, sat)
		container.append(to_hex(rgb_tuple[0], rgb_tuple[1], rgb_tuple[2]))

	return container

