from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in staging/__init__.py
from staging import __version__ as version

setup(
	name="staging",
	version=version,
	description="A simple app to add some additional CSS to your staging instances",
	author="Oliver Maklott",
	author_email="tinker@belowtoxic.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
